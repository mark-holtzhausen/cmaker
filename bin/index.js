#! /usr/bin/env node
'use strict';
const PWD = process.env.PWD;
const deepAssign = require('deep-assign');
const program = require('commander');
const fileHelpers = require('../src/lib/fileHelpers');
const path = require('path');
const util = require('util');
const fs = require('fs');
const MakeFile = require('../src/classes/MakeFile');
const watch = require('chokidar').watch;
const ffilter = require('filter-files');
const out = require('../src/lib/termOut');
const tc = require('cli-color');
const fileExists = require('file-exists');
const dateFormat = require('dateformat');

var configFile = 'cconfig.js';
var settingsOverride = {

};

out.clear();

program
    .arguments('[configFile]')
    .option('--root-folder <rootFolder>', 'Override the root folder')
    .option('--template-folder <templateFolder>', 'Override the template folder')
    .option('--output-folder <outputFolder>', 'Override the output folder')
    .option('-w, --watch', 'Watch config file for changes')
    .option('-f, --force', 'Generate files even if there are no changes to the existing output file')
    .action(function(file) {
        if (program.rootFolder) settingsOverride.rootFolder = path.resolve(PWD, program.rootFolder);
        if (program.templateFolder) settingsOverride.templateFolder = path.resolve(PWD, program.templateFolder);
        if (program.outputFolder) settingsOverride.outputFolder = path.resolve(PWD, program.outputFolder);
        if (program.force) settingsOverride.__force = true;

        if (file) {
            if (!path.isAbsolute(file)) {
                file = path.join(PWD, file)
            }
            if (fileHelpers.fileExists(file)) {
                configFile = file
            }
        }
    })
    .parse(process.argv)
    ;
const configRoot = path.dirname(path.resolve(configFile));
var config = {
    rootFolder: configRoot,
    templateFolder: configRoot,
    outputFolder: configRoot,
    __force: false
};
if (!path.isAbsolute(configFile)) configFile = path.join(config.rootFolder, configFile);

out.heading('CMaker')
if (!fileExists(configFile)) {
    out.write(tc.bold.red('\u26a0 Config file not specified\n'))
    process.exit(1);
}

// Load config


function processConfig(configFile, templateFile) {
    delete require.cache[require.resolve(configFile)]

    var currentConfig = deepAssign({}, config, require(configFile), settingsOverride);
    currentConfig = fileHelpers.fixFolders(currentConfig);
    if (templateFile) {
        new MakeFile(templateFile, currentConfig);
    } else {
        var files=ffilter.sync(currentConfig.templateFolder);
        for (var i = 0; i < files.length; i++) {
            new MakeFile(files[i], currentConfig);
        }
    }
    if (currentConfig.hasOwnProperty('deferredActions') && Array.isArray(currentConfig.deferredActions)) {
        currentConfig.deferredActions.forEach(function(action) {
            require('child_process').exec(action, (error, stdout, stderr) => {
                if (error) {
                    out.termError(stderr.trim(),`<Deferred> ${action}`);
                }else{
                    out.termText(stdout.trim(),`<Deferred> ${action}`);
                }
            })
        }, this);
        currentConfig.deferredActions=[];
    }
    return currentConfig;
}

out.write(tc.blue(`Processing config file ${tc.bold(configFile)}\n\n`));
var config = processConfig(configFile);

if (program.watch) {
    out.write(tc.green(`Watching for changes... \n\n`));
    var w = watch(configFile);
    w.add(config.templateFolder);
    w.on('change', (filepath) => {
        let currentTemplateFolder = config.templateFolder;
        out.write(`\n\n\n${dateFormat(Date.now(), "dddd, mmmm dS, yyyy, h:MM:ss TT")}${'-'.repeat(50)}`);
        if (filepath == configFile) {
            //reprocess all files
            config = processConfig(configFile);
            w.unwatch(config.templateFolder);
            w.add(config.templateFolder);
        } else {
            //reprocess only the changed file
            config = processConfig(configFile, filepath);
        }

    });
}
