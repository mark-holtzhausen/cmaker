# cmake

> A tool for generating config files from templates. A specified config file is
interpolated into a set of configuration templates.

[![NPM Version][npm-image]][npm-url]

## Install

```bash
npm i -g cmaker
```

## Usage

***cconfig.js***
```javascript
module.exports={
    // rootFolder:'',
    templateFolder: 'templates/',
    outputFolder: 'output/',
    user:{
        name: 'Peter',
        surname: 'Pan',
        email: 'peter.pan@neverland.org'
    }
}
```

***templates/someconfig.json***
```json
<cmake outputFile="someFolder/someFile.json" />
{
    "services":{
        "userService":{
            "nickName": "{{user.name}}",
            "fullName": "{{user.name}} {{user.surname}}",
            "authentication":{
                "username":"{{user.email}}"
            }
        }
    }
}
```
Invoke Cmake: `cmake cconfig.js`

This will result in ***output/somefolder/someFile.json***
```json
{
    "services":{
        "userService":{
            "nickName": "Peter",
            "fullName": "Peter Pan",
            "authentication":{
                "username":"peter.pan@neverland.org"
            }
        }
    }
}
```
## Options
 - **--help** Get help
 - **-w, --watch** Watch the config file and the templat folder for changes and
 render as changes occur
 - **-f, --force** Forces output even if files have not changed
 - **--root-folder \<rootFolder\>** Overrides root folder
 - **--template-folder \<templateFolder\>** Overrides template folder
 - **--output-folder \<outputFolder\>** Overrides output folder

## Features
 - Mustache used for templating. The template files should be valid Mustache templates.
 - Template folder is processed recursively so that you can organize a large
 number of templates into subfolders **without affecting their output folder**
 - Watching for changes to facilitate easy configuration change on multiple
services at once.
 - Command line overrides for **rootFolder** **templateFolder** and **outputFolder**
 - Only changed configs will be rendered.

## WishList
 - post-change triggers in `<cmake />` tag to allow service restart or other
 actions on output file change.

## ChangeLog
### V1.1.1
 - Fixed type in install section
 
### V1.1.0
 - Expose MakeFile function as an export
 - -f, --force flag to ensure output even when the resulting file has no changes.
 - updateAction trigger to execute shell command after a config file was 
 generated. Goes in the `ctag` definition of the template files.
 - deferredAction trigger to push a shell task onto a queue to be executed at 
 the end of all template renders.
## License

[MIT](http://vjpr.mit-license.org)

[npm-image]: https://img.shields.io/npm/v/cmaker.svg
[npm-url]: https://npmjs.org/package/cmaker
