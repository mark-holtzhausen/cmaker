module.exports={
    templateFolder:'templates/',
    outputFolder: 'output/',
    deferredActions: [
        'brew services restart varnish4'
    ],
    user:{
        name: 'Tlangelani Mboweni',
        age: 36,
        old:function(){
            return this.user.age>40; 
        }
    },
    t:{
        one: "targeting file 1",
        two: "targeting file 2"
    }
}
