'use strict';
const deepAssign = require('deep-assign');
const program = require('commander');
const path = require('path');
const util = require('util');
const fs = require('fs');
const parseString = require('xml2js').parseString;
const fileHelpers = require('../lib/fileHelpers');
const mustache = require('mustache');
const mkdirp = require('mkdirp');
const sha1 = require('sha1');
const fileExists = require('file-exists');
const out = require('../lib/termOut');
const tc = require('cli-color');



/**
 * MakeFile module
 */
class MakeFile {
    constructor(fileName, config) {
        this.oConfig = config;
        if (!this.oConfig.hasOwnProperty('deferredActions')) this.oConfig.deferredActions = [];
        this.config = deepAssign({}, config);
        this.fileName = path.resolve(this.config.templateFolder, fileName);
        this.config.outputFile = path.basename(this.fileName);
        this.loadConfig();

    }

    loadConfig() {
        this.fileContent = fs.readFileSync(this.fileName, 'utf8');
        let matches = /^(\<cmake\s+[\s\S]*\/>)\s*/m.exec(this.fileContent);
        this.config = fileHelpers.fixFolders(this.config);

        if (matches) {
            this.configString = matches[0];
            this.fileContent = this.fileContent.replace(this.configString, '');
            parseString(matches[1], (err, result) => {
                if (!err) {
                    for (var attr in result.cmake.$) {
                        this.config[attr] = result.cmake.$[attr]
                    }
                }
                this.config = fileHelpers.fixFolders(this.config);
                this.process()
            });
        }

    }

    process() {
        this.outputContent = mustache.render(this.fileContent, this.config);
        try {
            mkdirp.sync(path.dirname(this.config.outputFile));
            if (this.config.__force || (fileExists(this.config.outputFile) ? sha1(fs.readFileSync(this.config.outputFile, 'utf8')) != sha1(this.outputContent) : true)) {
                out.bullet(`Processing  ${tc.yellow(this.fileName.replace(this.config.templateFolder, ''))} => ${tc.green(this.config.outputFile.replace(this.config.rootFolder, ''))}`);
                fs.writeFileSync(this.config.outputFile, this.outputContent);
                if (this.config.hasOwnProperty('deferredAction')) {
                    this.oConfig.deferredActions.push(this.config.deferredAction)
                }
                if (this.config.hasOwnProperty('updateAction')) {
                    try{
                        out.termText(require('child_process').execSync(this.config.updateAction).toString(),this.config.updateAction);
                    }catch(e){
                        out.termError(e,this.config.updateAction);
                        console.error(`ERROR: ${e}`);
                    }
                }
            }

        } catch (e) {

        }
    }
}

module.exports = MakeFile;
