'use strict';
const fs = require('fs');
const path = require('path');
const util = require('util');
/**
 * fileHelper module
 */
class fileHelper {
    constructor() {
        return this.constructor;
    }

    static fileExists(fileName) {
        try {
            return fs.statSync(fileName).isFile();
        } catch (e) {
            return false;
        }
    }

    static fixFolders(config){
        config.rootFolder=path.resolve(config.rootFolder);
        if(!path.isAbsolute(config.templateFolder)) config.templateFolder=path.resolve(config.rootFolder,config.templateFolder);
        if(!path.isAbsolute(config.outputFolder)) config.outputFolder=path.resolve(config.rootFolder, config.outputFolder);
        if(config.hasOwnProperty('outputFile')) config.outputFile=path.resolve(config.outputFolder, config.outputFile);
        // console.log('path.resolve(%s,%s)=%s',config.outputFolder, config.outputFile,path.resolve(config.outputFolder, config.outputFile || ''));
        return config; 
    }

}

module.exports = fileHelper;
