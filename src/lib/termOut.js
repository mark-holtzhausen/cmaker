'use strict';
const tc=require('cli-color');
/**
 * TermOut module
 */
class TermOut{
    constructor(){
        return this.constructor;
    }

    static heading(text){
        TermOut.write(tc.bold(`\n${text}`));
        TermOut.write(tc.bold('='.repeat(text.length)));
        TermOut.write();
    }
    
    static subHeading(text){
        TermOut.write(`\n${text}`);
        TermOut.write('-'.repeat(text.length));
    }

    static bullet(text,padding,icon){
        icon=icon||'\u26AC';
        padding=' '.repeat(padding||3)
        TermOut.write(`${padding}${icon} ${text}`)
    }

    static write(text){
        text=text||'';
        process.stdout.write(`${text}\n`);
    }

    static indent(text,spaces){
        spaces=spaces || 4;
        return text.split('\n').map(part=>`${' '.repeat(spaces)}${part}`).join('\n')
    }

    static termText(text,cmd){
        cmd=cmd||'';
        text=text.trim()||'';
        let c=tc.xterm(117);

        let termString=`Terminal ${cmd?`(${cmd})`:''}`;

        if(text){
            var out=c.bold(`\n${termString}${'-'.repeat(70 - termString.length)}\n`) + c(TermOut.indent(text)) + c.bold(`\n${'-'.repeat(70)}`)
            process.stdout.write(TermOut.indent(out,6)+'\n\n')
        }
    }

    static termError(text,cmd){
        cmd=cmd||'';
        text=text.trim()||'';
        let c=tc.xterm(167);

        let termString=`Terminal ${cmd?`(${cmd})`:''}`;

        if(text){
            var out=c.bold(`\n${termString}${'-'.repeat(70 - termString.length)}\n`) + c(TermOut.indent(text)) + c.bold(`\n${'-'.repeat(70)}`)
            process.stdout.write(TermOut.indent(out,6)+'\n\n')
        }
    }

    static clear(){
        process.stdout.write(tc.reset);
    }

}

const singleton=new TermOut;
module.exports = singleton;
